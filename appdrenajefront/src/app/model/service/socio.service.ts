import { Injectable } from '@angular/core';
import { URL_BACKEND } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Socio } from '../class/socio';
import { map, catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SocioService {
  private urlEndPoint:string=URL_BACKEND+'/api-drenaje/socio';
  constructor(private http:HttpClient) { }

  public store(socio:Socio):Observable<Socio>{
    
    return this.http.post<Socio>(`${this.urlEndPoint}`,socio).pipe(
      map((response:any)=>response.socio as Socio),
      catchError(
        e=>{
          if(e.status==400){
            return throwError(e);
          }
          Swal.fire(e.error.mensaje,e.error.err,'error');
          return throwError(e);
        }
      )
    );
  }

  public getById(id:number):Observable<Socio>{
    return this.http.get<Socio>(`${this.urlEndPoint}/find-socio/${id}`);
  }

  public getAll():Observable<Socio[]>{
    return this.http.get<Socio[]>(`${this.urlEndPoint}`);
  }

  public findAllBySector(sector:number):Observable<Socio[]>{
    return this.http.get<Socio[]>(`${this.urlEndPoint}/sector/${sector}`);
  }

  public findAllByGenero(genero:string):Observable<Socio[]>{
    return this.http.get<Socio[]>(`${this.urlEndPoint}/${genero}`);
  }

  public findAllBySectorAndGenero(genero:string,sector:number):Observable<Socio[]>{
    return this.http.get<Socio[]>(`${this.urlEndPoint}/genero-sector/${genero}/${sector}`);
  }

  public update(socio:Socio):Observable<Socio>{
    return this.http.put<Socio>(`${this.urlEndPoint}/${socio.id}`,socio).pipe(
      map((response:any)=>response.socio as Socio),
      catchError(err=>{
        if(err.status==400){
          return throwError(err);
        }
        Swal.fire(err.error.mensaje,err.error.err,'error');
        return throwError(err);
      })
    )
  }
}
