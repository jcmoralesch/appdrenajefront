import { Component, OnInit } from '@angular/core';
import { ProveedorService } from '../../../model/service/proveedor.service';
import { Proveedor } from '../../../model/class/proveedor';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-proveedor',
  templateUrl: './form-proveedor.component.html',
  styleUrls: ['./form-proveedor.component.css']
})
export class FormProveedorComponent implements OnInit {

  public proveedor:Proveedor= new Proveedor();
  public loading:boolean=false;

  constructor(public  proveedorService:ProveedorService,
              private toastr:ToastrService) { }

  ngOnInit() {
  }

  public store():void{
    this.loading=true;

    this.proveedorService.store(this.proveedor).subscribe(
      response=>{
            this.proveedorService.notificarCambio.emit(response);
            this.loading=false;
            this.cerrarModal();
            this.toastr.success('Proveedor registrado con exito','Success');
      },
      err=>{
        this.loading=false;
        Swal.fire('Error', 'No fue posible realizar la operacion','error');
      }
    )
    
  }

  public cerrarModal():void{
    this.proveedorService.cerrarModal();
  }

}
