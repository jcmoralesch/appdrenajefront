import { Component, OnInit, ViewChild } from '@angular/core';
import { PagoService } from '../../../model/service/pago.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { PagoCuota } from '../../../model/class/pago-cuota';
import { TipoCuota } from '../../../model/class/tipo-cuota';
import { TipoCuotaService } from '../../../model/service/tipo-cuota.service';
import Swal from 'sweetalert2';
import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { LoginService } from '../../../model/service/login.service';

@Component({
  selector: 'app-historial-pago',
  templateUrl: './historial-pago.component.html',
  styleUrls: ['./historial-pago.component.css']
})
export class HistorialPagoComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource:any;
  public totalPago:number;
  public tipoCuotas:TipoCuota[];

  constructor(private pagoService:PagoService,
              private tipoCuotaService:TipoCuotaService,
              public loginService:LoginService) { }

  ngOnInit(): void {
    this.findAllPagosByCurrentDate();
    this.cargarTipoCuota();
  }

  displayedColumns = ['Codigo','Nombre','Apellido','Cantidad','Fecha','Hora','TipoCuota'];

  private findAllPagosByCurrentDate():void{
    this.pagoService.findByCurrentDate().subscribe(
      response=>{
        this.totalPago=  response.map(t => t.cantidad).reduce((acc, value) => acc + value, 0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();
      }
    )
  }

  public tableFilter(): (data: PagoCuota, filter: string) => boolean {
    let filterFunction = function (data, filter): boolean {
      return (
        data.cuota.socio.nombre.indexOf(filter) != -1 ||
        data.cuota.socio.apellido.indexOf(filter) != -1
      );
    };
    return filterFunction;
  }

  public applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter = filterValue;
  }

  private cargarTipoCuota():void{
    this.tipoCuotaService.getAll().subscribe(
      response=>this.tipoCuotas=response
    )
  }

  public consultarPagosRangoFechas(f1,f2,tp):void{
    if(f1=='' || f2==''){
      Swal.fire('No se puede realizar la consulta','Especifique las fechas para realizar la consulta','warning');
    }else{
      if(tp=='---Seleccionar Tipo cuota-----'){
        tp='undefined';
      }
      this.pagoService.findByfechaBeetwenfecha(f1,f2,tp).subscribe(
        response=>{
          this.totalPago=  response.map(t => t.cantidad).reduce((acc, value) => acc + value, 0);
          this.dataSource = new MatTableDataSource();
          this.dataSource.data=response;
          this.dataSource.sort=this.sort;
          this.dataSource.paginator=this.paginator;
          this.dataSource.filterPredicate = this.tableFilter();
        }
      )}
  }

  /* *************************************************PDF  */
  
  public generarPDF():void{
    let doc = new jsPDF();
    doc.setFontSize(10);
    doc.text(`Reporte sobre pagos de cuotas `,15, 15);
    
    doc.autoTable({
      head: this.headRows(),
      body: this.bodyRows(),
      foot: this.footRows(),
      startY: 17,
      theme:'grid',
      showHead: "firstPage",
      columnStyles: {
        cantidad:{
          fontStyle: 'bold'
        }
    },
      styles: { cellPadding: 0.5, fontSize: 8 }
    });
    doc.save(`PagoCuota`);
  }

  private   headRows() {
    return [
      {
        numero:'#',
        nombre: "Nombre",
        apellido: "Apellido",
        cantidad:"Cantidad",
        fecha:"Fecha",
        tipoCuota:"Tipo Cuota",
        opcion:"----"
      }
    ];
  }

  private bodyRows() {
    let body = [];
    let contador: number = 0;
    let t = this.dataSource.data.length;
    for (var j = 1; j <= t; j++) {
      body.push({
        numero:j,
        nombre: this.dataSource.data[contador].cuota.socio.nombre,
        apellido:this.dataSource.data[contador].cuota.socio.apellido,
        cantidad: this.dataSource.data[contador].cantidad,
        fecha: this.dataSource.data[contador].fecha,
        tipoCuota: this.dataSource.data[contador].cuota.tipoCuota.tipoCuota,
        opcion:' '

      });
      contador++;
    }
    return body;
  }

  private   footRows() {
    return [
      {
        numero:'',
        nombre: "Total",
        apellido: "Pagos",
        cantidad:this.totalPago,
        fecha:"",
        tipoCuota:'',
        opcion:""
      }
    ];
  }

}
