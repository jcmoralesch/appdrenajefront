import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../../config/config';
import { Cuota } from '../class/cuota';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class CuotaService {

  private urlEndPoint:string=URL_BACKEND+'/api-drenaje/cuota';
  public modal:boolean=false;

  constructor(private http:HttpClient) { }

  
  public store(cuota:Cuota):Observable<Cuota>{
    return this.http.post<Cuota>(`${this.urlEndPoint}`,cuota).pipe(
      map((response:any)=>response.cuota as Cuota),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public storeCoutaIndividual(cuota:Cuota):Observable<Cuota>{
    return this.http.post<Cuota>(`${this.urlEndPoint}/cuota/individual`,cuota).pipe(
      map((response:any)=>response.cuota as Cuota),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public findByEstadoCuota():Observable<Cuota[]>{
    return this.http.get<Cuota[]>(`${this.urlEndPoint}`);
  }

  
  filtrarSocios(term: string): Observable<Cuota[]> {
    return this.http.get<Cuota[]>(`${this.urlEndPoint}/filtrar-cuota/${term}`);
  }

  public findByTipoCuota(tipoCuota:string):Observable<Cuota[]>{
    return this.http.get<Cuota[]>(`${this.urlEndPoint}/tipo-cuota/${tipoCuota}`);
  }

  public findAllByTipoCuota(tipoCuota:string):Observable<Cuota[]>{
    return this.http.get<Cuota[]>(`${this.urlEndPoint}/tipo/all/cuota/${tipoCuota}`);
  }

  public updateEstdoPago():Observable<Cuota[]>{
    return this.http.get<Cuota[]>(`${this.urlEndPoint}/update/estado/pago`);
  }

  public findByEstadoCuotaPendiente(tipoCuota:string):Observable<Cuota[]>{
    return this.http.get<Cuota[]>(`${this.urlEndPoint}/find-estado-pago/pendiente/${tipoCuota}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
