import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../../config/config';
import { Observable } from 'rxjs';
import { Sector } from '../class/sector';

@Injectable({
  providedIn: 'root'
})
export class SectorService {
  
  private urlEndPoint:string=URL_BACKEND+'/api-drenaje/sector';
  constructor(private http:HttpClient) { }

  public getAll():Observable<Sector[]>{
    return this.http.get<Sector[]>(`${this.urlEndPoint}`);
  }
}
