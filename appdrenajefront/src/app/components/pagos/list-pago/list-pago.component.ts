import { Component, OnInit, ViewChild } from '@angular/core';
import { PagoService } from '../../../model/service/pago.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-list-pago',
  templateUrl: './list-pago.component.html',
  styleUrls: ['./list-pago.component.css']
})
export class ListPagoComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource:any;

  constructor(private pagoService:PagoService) { }

  ngOnInit() {
     this.getByCurrentDate();
     this.cargarDespuesDeInsertar();
  }

  displayedColumns = ['Codigo','Nombre','Apellido','Cantidad','Hora'];

  private getByCurrentDate():void{
    this.pagoService.findByCurrentDate().subscribe(
      response=>{
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
      }
    )
  }

  private cargarDespuesDeInsertar():void{
    this.pagoService.notificarCambio.subscribe(
      response=>this.getByCurrentDate()
    )
  }

}
