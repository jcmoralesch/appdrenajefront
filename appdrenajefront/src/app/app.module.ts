import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/usuario/login/login.component';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/nav/header/header.component';
import { FormUsuarioComponent } from './components/usuario/form-usuario/form-usuario.component';
import { UsuarioComponent } from './components/usuario/usuario/usuario.component';
import { TokenInterceptor } from './model/service/interceptors/token.interceptor';
import { AuthInterceptor } from './model/service/interceptors/auth.interceptor';
import { FormSocioComponent } from './components/socios/form-socio/form-socio.component';
import { ListSociosComponent } from './components/socios/list-socios/list-socios.component';
import { DetalleSocioComponent } from './components/socios/detalle-socio/detalle-socio.component';
import {MatTableModule,MatPaginatorModule,MatSortModule,MatButtonToggleModule,MatFormFieldModule,MatInputModule} from '@angular/material/';
import {MatNativeDateModule} from '@angular/material';
import { FormCuotaComponent } from './components/cuotas/form-cuota/form-cuota.component';
import { ListCuotaComponent } from './components/cuotas/list-cuota/list-cuota.component';
import { TipoCuotaFormComponent } from './components/tipoCuota/tipo-cuota-form/tipo-cuota-form.component';
import { FormFechaComponent } from './components/fechaPagos/form-fecha/form-fecha.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { registerLocaleData } from '@angular/common';
import localeESGTQ from '@angular/common/locales/es-GT';
import { ListTipoCuotaComponent } from './components/tipoCuota/list-tipo-cuota/list-tipo-cuota.component';
import { FormPagoComponent } from './components/pagos/form-pago/form-pago.component';
import { ListPagoComponent } from './components/pagos/list-pago/list-pago.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { FormGastoComponent } from './components/gastos/form-gasto/form-gasto.component';
import { ListGastoComponent } from './components/gastos/list-gasto/list-gasto.component';
import { FormProveedorComponent } from './components/proveedors/form-proveedor/form-proveedor.component';
import { ItemGastoComponent } from './components/item-gasto/item-gasto.component';
import { HistorialPagoComponent } from './components/pagos/historial-pago/historial-pago.component';
import { InformeSaldoComponent } from './components/informe-saldo/informe-saldo.component';

registerLocaleData(localeESGTQ, 'es-GT');


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FormUsuarioComponent,
    UsuarioComponent,
    FormSocioComponent,
    ListSociosComponent,
    DetalleSocioComponent,
    FormCuotaComponent,
    ListCuotaComponent,
    TipoCuotaFormComponent,
    FormFechaComponent,
    ListTipoCuotaComponent,
    FormPagoComponent,
    ListPagoComponent,
    FormGastoComponent,
    ListGastoComponent,
    FormProveedorComponent,
    ItemGastoComponent,
    HistorialPagoComponent,
    InformeSaldoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(
      {
        closeButton:true
      }
    ),
    ToastContainerModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
  ],
  providers: [ {provide: HTTP_INTERCEPTORS,useClass:TokenInterceptor,multi:true},
               {provide: HTTP_INTERCEPTORS,useClass:AuthInterceptor,multi:true},
               { provide: LOCALE_ID, useValue: 'es-GT' },
              ],
  bootstrap: [AppComponent]
})
export class AppModule { }


