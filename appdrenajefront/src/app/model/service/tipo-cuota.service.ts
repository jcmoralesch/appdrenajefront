import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../../config/config';
import { TipoCuota } from '../class/tipo-cuota';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TipoCuotaService {

  private urlEndPoint:string=URL_BACKEND+'/api-drenaje/tipo-cuota';
  private _notificarCambio= new EventEmitter<any>();

  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  constructor(private http:HttpClient) { }

  public store(tipoCuota:TipoCuota):Observable<TipoCuota>{
    return this.http.post<TipoCuota>(`${this.urlEndPoint}`,tipoCuota).pipe(
      map((response:any)=>response.tipoCuota as TipoCuota),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public getAll():Observable<TipoCuota[]>{
    return this.http.get<TipoCuota[]>(`${this.urlEndPoint}`);
  }




}
