import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../model/service/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public loginService:LoginService,private router:Router,private toastr:ToastrService) { }

  ngOnInit() {
  }


 public  logout():void{
    this.loginService.logout();
    this.router.navigate(['/login']);
    this.toastr.info("Session finalizada", 'Logout')
  }

}
