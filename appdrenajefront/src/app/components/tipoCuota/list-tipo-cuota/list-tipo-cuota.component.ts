import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../model/service/login.service';

@Component({
  selector: 'app-list-tipo-cuota',
  templateUrl: './list-tipo-cuota.component.html',
  styleUrls: ['./list-tipo-cuota.component.css']
})
export class ListTipoCuotaComponent implements OnInit {

  constructor(public loginService:LoginService) { }

  ngOnInit() {
  }

}
