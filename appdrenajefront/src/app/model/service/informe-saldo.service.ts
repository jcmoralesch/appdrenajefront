import { Injectable,EventEmitter } from '@angular/core';
import { URL_BACKEND } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InformeSaldo } from '../class/informe-saldo';

@Injectable({
  providedIn: 'root'
})
export class InformeSaldoService {

  private urlEndPoint:string=URL_BACKEND+'/api-drenaje';
  private _notificarCambio= new EventEmitter<any>();

  constructor(private http:HttpClient) { }

  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  public compactarInformeSaldo():Observable<InformeSaldo>{
    return this.http.get<InformeSaldo>(`${this.urlEndPoint}/informe-saldo`);
  }

  public informeSaldoReporte():Observable<InformeSaldo>{
    return this.http.get<InformeSaldo>(`${this.urlEndPoint}/informe-saldo/reporte`);
  }

  public getAllOrderByDesc():Observable<InformeSaldo[]>{
    return this.http.get<InformeSaldo[]>(`${this.urlEndPoint}/informe-saldo/find/all`);
  }
}
