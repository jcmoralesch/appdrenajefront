import { Component, OnInit, ViewChild } from "@angular/core";
import { SocioService } from "../../../model/service/socio.service";
import { MatSort, MatPaginator, MatTableDataSource } from "@angular/material";
import { Socio } from "../../../model/class/socio";
import { SectorService } from "../../../model/service/sector.service";
import { Sector } from "../../../model/class/sector";
import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { CuotaService } from '../../../model/service/cuota.service';
import { Cuota } from 'src/app/model/class/cuota';
import { TipoCuota } from 'src/app/model/class/tipo-cuota';
import { TipoCuotaService } from '../../../model/service/tipo-cuota.service';
import Swal from 'sweetalert2';
import { LoginService } from '../../../model/service/login.service';

@Component({
  selector: "app-list-socios",
  templateUrl: "./list-socios.component.html",
  styleUrls: ["./list-socios.component.css"],
})
export class ListSociosComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  public dataSource: any;
  public sectors: Sector[];
  public generoIsValid: boolean = false;
  public sectorIsValid: boolean = false;
  private valGenero: string;
  private valSector: number;
  public listaSocios:Socio[];
  private fecha:Date= new Date()
  private mes;
  public fechaActual;
  public socio:Socio = new Socio();
  public cuota:Cuota = new Cuota();
  public tipoCuotas:TipoCuota[];
  private diaFecha:any;

  constructor(
    private socioService: SocioService,
    private sectorService: SectorService,
    public cuotaService:CuotaService,
    private tipoCuotaService:TipoCuotaService,
    public loginService:LoginService
  ) {}

  ngOnInit() {
    this.getAll();
    this.getAllSector();
    this.setearFecha();
    this.getAllTipoCuota();

    this.setearFecha()
  }

  displayedColumns = [
    "Codigo",
    "Nombre",
    "Apellido",
    "Celular",
    "Sector",
    "Sexo",
    "Registro",
    "Acciones",
  ];

  private setearFecha():void{
    this.mes=this.fecha.getMonth()+1;
    if(this.mes<10){
      this.mes='0'+this.mes;
    }
    if(this.fecha.getDate()<10){
         this.diaFecha='0'+this.fecha.getDate();
    }else{
      this.diaFecha=this.fecha.getDate();
    }
    //this.fechaActual =this.fecha.getFullYear()+'-'+this.mes+'-'+ this.fecha.getDate();
    this.fechaActual =this.fecha.getFullYear()+'-'+this.mes+'-'+ this.diaFecha;
  }

  public getAll(): void {
    this.listaSocios=[];
    this.socioService.getAll().subscribe((response) => {
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = response; 
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.filterPredicate = this.tableFilter();
      this.listaSocios=response;
    });
  }

  applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter = filterValue;
  }

  public tableFilter(): (data: Socio, filter: string) => boolean {
    let filterFunction = function (data, filter): boolean {
      return (
        data.nombre.indexOf(filter) != -1 || data.apellido.indexOf(filter) != -1
      );
    };
    return filterFunction;
  }

  private getAllSector(): void {
    this.sectorService
      .getAll()
      .subscribe((response) => (this.sectors = response));
  }

  public findAllBySector(event: number): void {
    this.valSector = event;
    this.socioService.findAllBySector(event).subscribe((response) => {
      this.sectorIsValid = true;
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = response;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.filterPredicate = this.tableFilter();
    });
  }

  public findAllByGenero(event: string): void {
    this.valGenero = event;
    this.socioService.findAllByGenero(event).subscribe((response) => {
      this.generoIsValid = true;
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = response;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.filterPredicate = this.tableFilter();
    });
  }

  public consultarGeneroSector(): void {
    this.socioService
      .findAllBySectorAndGenero(this.valGenero, this.valSector)
      .subscribe((response) => {
        this.generoIsValid = true;
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.filterPredicate = this.tableFilter();
      });
  }

  public modalAsignarCuota(row):void{
    this.cuotaService.abrirModal();
    this.socio=row;
  }

  public cerrarModalCuota():void{
    this.cuotaService.cerrarModal();
  }

  public asignarCuota():void{
    this.cuota.socio=this.socio;
    this.cuotaService.storeCoutaIndividual(this.cuota).subscribe(
      response=>{
        this.cerrarModalCuota();
        Swal.fire('Exito','Couta asignda correctamente','success')
      },
      err=>Swal.fire('Error','No fue posible asignar la cuota','error')
    )
  }

  private getAllTipoCuota():void{
    this.tipoCuotaService.getAll().subscribe(
      tipoCuota=>this.tipoCuotas=tipoCuota
    )
  }

  /**************************GENERAR PDF************************** */
  generarpdf() {
    let doc = new jsPDF();
    doc.setFontSize(10);
    doc.text(`LISTADO DE USUARIOS DE PROYECTO DE DRENAJE`,15, 15);
    
    doc.autoTable({
      head: this.headRows(),
      body: this.bodyRows(),
      startY: 17,
      theme:'grid',
      showHead: "firstPage",
      styles: { cellPadding: 0.5, fontSize: 8 }
    });
    doc.save(`ListadoUsuarios`);
  }

  private   headRows() {
    return [
      {
        codigo: "#",
        nombre: "Nombre",
        apellido: "Apellido",
        telefono: "Teléfono",
        sector: "Sector",
        opcion:"----",
        opcion1:"----",
        opcion2:"----",
        opcion3:"----",
        opcion4:"----"
      }
    ];
  }

   bodyRows() {
    let body = [];
    let contador: number = 0;
    let t = this.listaSocios.length;
    for (var j = 1; j <= t; j++) {
      body.push({
        codigo: this.listaSocios[contador].codigo,
        nombre: this.listaSocios[contador].nombre,
        apellido:this.listaSocios[contador].apellido,
        telefono: this.listaSocios[contador].telefono,
        sector: this.listaSocios[contador].sector.sector,
        opcion:' ',
        opcion1:' ',
        opcion2:' ',
        opcion3:' ',
        opcion4:' '

      });
      contador++;
    }
    return body;
  }

  /************************************************************************************************************* */
}
