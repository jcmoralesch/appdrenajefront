import { Component, OnInit, ViewChild } from "@angular/core";
import { Gasto } from "../../../model/class/gasto";
import { FormControl, NgForm } from "@angular/forms";
import { Proveedor } from "src/app/model/class/proveedor";
import { Observable } from "rxjs";
import {
  MatAutocompleteSelectedEvent,
  MatSort,
  MatPaginator,
  MatTableDataSource,
} from "@angular/material";
import { ProveedorService } from "../../../model/service/proveedor.service";
import { ItemGastoService } from "../../../model/service/item-gasto.service";
import { ItemGasto } from "../../../model/class/item-gasto";
import { GastoService } from "../../../model/service/gasto.service";
import Swal from "sweetalert2";
import { map, flatMap } from "rxjs/operators";

@Component({
  selector: "app-form-gasto",
  templateUrl: "./form-gasto.component.html",
  styleUrls: ["./form-gasto.component.css"],
})
export class FormGastoComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource: any;

  public gasto: Gasto = new Gasto();
  public loading: false;
  public autocompleteControlProveedor = new FormControl();
  public proveedoresFiltrados: Observable<Proveedor[]>;
  public proveedorNew: Proveedor = new Proveedor();
  public itemGasto: ItemGasto = new ItemGasto();
  public totalGasto: number;
  public gastoEnviar: Gasto;

  constructor(
    private proveedorService: ProveedorService,
    public itemGastoService: ItemGastoService,
    private gastoService: GastoService
  ) {}

  ngOnInit() {
    this.proveedoresFiltrados = this.autocompleteControlProveedor.valueChanges.pipe(
      map((value) => (typeof value === "string" ? value : value.nombre)),
      flatMap((value) => (value ? this._filterproveedor(value) : []))
    );
    this.cargarProveedorDeseModal();
    this.findByCurrentDate();
    this.cargarListGasto();
  }

  private _filterproveedor(value: string): Observable<Proveedor[]> {
    const filterValue = value.toUpperCase();
    return this.proveedorService.filtrarProveedores(filterValue);
  }

  private findByCurrentDate(): void {
    this.gastoService.findByCurrenDate().subscribe((response) => {
      this.totalGasto = response
        .map((t) => t.total)
        .reduce((acc, value) => acc + value, 0);
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = response;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  displayedColumns = [
    "Id",
    "NoFactura",
    "Proveedor",
    "Hora",
    "TGasto",
    "Acciones",
  ];

  public mostrardatosProveedor(proveedor?: Proveedor): string | undefined {
    return proveedor ? proveedor.nombreCompleto : undefined;
  }

  public seleccionarProveedor(event: MatAutocompleteSelectedEvent): void {
    const proveedor = event.option.value as Proveedor;
    this.proveedorNew = proveedor;
    this.autocompleteControlProveedor.setValue("");
    event.option.focus();
    event.option.deselect();
  }

  public cargarProveedorDeseModal(): void {
    this.proveedorService.notificarCambio.subscribe(
      (response) => (this.proveedorNew = response)
    );
  }

  public cerrarModalItemGasto(): void {
    this.itemGastoService.cerrarModal();
  }

  public abrirModalItemGasto(): void {
    this.itemGastoService.abrirModal();
  }

  public store(gasto: NgForm): void {
    if (gasto.form.value.nombre == "") {
      this.gasto.proveedor = null;
    } else {
      this.gasto.proveedor = this.proveedorNew;
    }

    this.gastoService.store(this.gasto).subscribe((response) => {
      this.gastoService.notificarCambio.emit();
      Swal.fire("Exito", "Gastos realizados con exito", "success");
      let totalElementos: number = this.gasto.items.length;
      let i = 0;
      for (i; i <= totalElementos; i++) {
        this.gasto.items.pop();
      }

      this.proveedorNew.nombreCompleto = "";
      this.proveedorNew.id = 0;
      this.gasto.noFactura = null;
    });
  }

  public agregarItems(itemForm: NgForm): void {
    this.itemGasto.subTotalCompra =
      this.itemGasto.cantidad * this.itemGasto.precioUnitario;
    this.gasto.items.push(this.itemGasto);
    this.itemGastoService.cerrarModal();
    this.itemGasto = new ItemGasto();
  }

  public abrirModalProveedor(): void {
    this.proveedorService.abrirModal();
  }

  public eliminarItemGasto(item: ItemGasto): void {
    const posicionElemento: number = this.gasto.items.indexOf(item);
    this.gasto.items.splice(posicionElemento, 1);
  }

  public calcularGranTotal(): number {
    let totalGasto = 0;
    this.gasto.items.forEach((item: ItemGasto) => {
      totalGasto += item.subTotalCompra;
    });
    return totalGasto;
  }

  private cargarListGasto(): void {
    this.gastoService.notificarCambio.subscribe((response) =>
      this.findByCurrentDate()
    );
  }

  public abrirModalDetalle(row): void {
    this.gastoEnviar = row;
    this.itemGastoService.abrirModalDetalle();
  }
}
