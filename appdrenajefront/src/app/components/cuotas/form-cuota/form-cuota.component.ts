import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Cuota } from '../../../model/class/cuota';
import { TipoCuota } from '../../../model/class/tipo-cuota';
import { TipoCuotaService } from '../../../model/service/tipo-cuota.service';
import { FechaPagosService } from '../../../model/service/fecha-pagos.service';
import { CuotaService } from '../../../model/service/cuota.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-cuota',
  templateUrl: './form-cuota.component.html',
  styleUrls: ['./form-cuota.component.css']
})
export class FormCuotaComponent implements OnInit {

  public cuota:Cuota= new Cuota();
  public loading:boolean=false;
  public tipoCuotas:TipoCuota[];

  constructor(private tipoCuotaService:TipoCuotaService,
    public fechaPagosService:FechaPagosService,
    private cuotaService:CuotaService,
    private router:Router) { }

  ngOnInit() {
    this.getAllTipoCuota();
  }

  public store(cuotaForm:NgForm):void{
      this.loading=true;
      this.cuotaService.store(this.cuota).subscribe(
        response=>{
          this.loading=false;
          Swal.fire('Exito','Cuotas asignadas a todos los usuarios del proyecto','success');
          this.router.navigate(['/cuota']);
        },
        err=>{
          Swal.fire('Error','No fue posible asignar las cuotas','error');
          this.loading=false;
        }
      )

  }

  private getAllTipoCuota():void{
    this.tipoCuotaService.getAll().subscribe(
      tipoCuota=>this.tipoCuotas=tipoCuota
    )
  }

  compararTipoCuota(o1:TipoCuota,o2:TipoCuota):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

}
