import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ItemGastoService {

  public modal:boolean=false;
  public modalDetalle:boolean=false;

  constructor() { }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }

  public abrirModalDetalle():void{
    this.modalDetalle=true;
  }

  public cerrarModalDetalle():void{
    this.modalDetalle=false;
  }
}
