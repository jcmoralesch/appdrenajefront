import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from 'src/app/model/service/login.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  
  constructor(private loginService:LoginService,private router:Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(!this.loginService.isAuthenticated()){
        this.router.navigate(['/login']);
        return false;
    }

    console.log(next)
    let role=next.data['role'] as string;
    if (this.loginService.hasRole(role)){
      return true;
    }
    this.router.navigate(['/form-usuario']);
    return false;
  }
  
}
