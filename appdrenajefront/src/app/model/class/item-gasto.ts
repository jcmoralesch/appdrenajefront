export class ItemGasto {

    public cantidad:number;
    public descripcion:string;
    public precioUnitario:number;
    public subTotalCompra:number=0;
    public id:number;
}
