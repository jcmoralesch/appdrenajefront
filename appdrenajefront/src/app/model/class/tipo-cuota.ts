import { FechasPago } from './fechas-pago';

export class TipoCuota {
    public id:number;
    public tipoCuota:string;
    public descripcion:string;
    public items:Array<FechasPago> =[];
    public noPagos:number;
}
