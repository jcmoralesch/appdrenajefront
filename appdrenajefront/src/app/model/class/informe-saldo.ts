export class InformeSaldo {
    public fechaLectura;
    public saldoAnterior;
    public saldoEnCaja;
    public totalGasto;
    public totalRecaudo;
    public id:number;
    public fecha;
}
