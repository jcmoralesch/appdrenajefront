import { Component, OnInit } from '@angular/core';
import { PagoCuota } from '../../../model/class/pago-cuota';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Cuota } from 'src/app/model/class/cuota';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { flatMap, map } from 'rxjs/operators';
import { CuotaService } from '../../../model/service/cuota.service';
import Swal from 'sweetalert2';
import { PagoService } from '../../../model/service/pago.service';
import { ToastrService } from 'ngx-toastr';
import { TipoCuota } from '../../../model/class/tipo-cuota';
import { TipoCuotaService } from '../../../model/service/tipo-cuota.service';
import { LoginService } from '../../../model/service/login.service';
import * as jsPDF from "jspdf";
import { CurrencyPipe } from '@angular/common';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-form-pago',
  templateUrl: './form-pago.component.html',
  styleUrls: ['./form-pago.component.css']
})
export class FormPagoComponent implements OnInit {

  pagosArray:Array<PagoCuota>=[];
  autocompleteControl = new FormControl();
  cuotasFiltrados: Observable<Cuota[]>;
  public loading:boolean=false;
  public tipoCuotas:TipoCuota[];
  public cuotasPendientes:Cuota[];
  currencyPipe= new CurrencyPipe('es-GT');
  datePipe = new DatePipe('es-GT');

  constructor(public cuotaService:CuotaService,
              private pagoCuotaService:PagoService,
              private toastr:ToastrService,
              private tipoCuotaService:TipoCuotaService,
              public loginService:LoginService) {
    
   }

  ngOnInit() {
    this.cargarCuotas();
    this.cargarTipoCuota();
  }

  private cargarCuotas():void{
    this.cuotasFiltrados = this.autocompleteControl.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.socio.nombre), 
        flatMap(value => value ? this._filter(value) : [])
      );
  }

  private _filter(value: string): Observable<Cuota[]> {
    const filterValue = value.toUpperCase();
    return this.cuotaService.filtrarSocios(filterValue);
  }

  mostrarNombre(cuota?: Cuota): string | undefined {
    return cuota ? cuota.socio.nombre : undefined;
  }

  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
    const cuota = event.option.value as Cuota;
   
    const nuevoItem = new PagoCuota();
    nuevoItem.cuota = cuota;
    //this.pagosArray.push(nuevoItem);
    this.pagosArray.unshift(nuevoItem);
    this.autocompleteControl.setValue(''); 
    event.option.focus();
    event.option.deselect();

  }

  public eliminarItem(id:number):void{
    this.pagosArray = this.pagosArray.filter((item: PagoCuota) => id !== item.cuota.id);
  }

  public actualizarCuota(id: number, event: any): void {
    let cuota= event.target.value;
    this.pagosArray = this.pagosArray.map((item: PagoCuota) => {
      if (id === item.cuota.id) {
        item.cantidad= parseFloat(cuota);
        item.cantidad=item.cantidad;
      }
      return item;
    });
  }

  public calcularGranTotal():number{
    let total:number=0;
    this.pagosArray.forEach((item: PagoCuota)=>{
        total += item.cantidad;
    });
    return total;
}

 public  create(pagoForm):void{

    if (this.pagosArray.length == 0) {
      this.autocompleteControl.setErrors({ 'invalid' : true });
    } 
    if (pagoForm.form.valid && this.pagosArray.length > 0) {
    this.loading=true;
    
    this.pagoCuotaService.store(this.pagosArray).subscribe(
      response=>{
        this.loading=false;
        this.pagoCuotaService.notificarCambio.emit();
        Swal.fire('Exito','Cuotas registradas satisfactoriamente','success');
        let i=0;
        let cantidad=this.pagosArray.length;

        this.generarPDF(response);

        for(i;i<=cantidad;i++){
          this.pagosArray.pop();
        }
        this.cargarCuotas();
      },
      err=>{
        this.loading=false;
        Swal.fire('Error', 'No fue posible registrar el pago de cuota', 'error');
      }
    )
    }
  }

  public updateEstadoPago():void{
    Swal.fire({
       title:'Seguro que desea?',
      text: 'Resetear estado pagos',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI, Resetear !'
    }).then((result) => {

      if (result.value) {
        this.loading=true;
        this.cuotaService.updateEstdoPago().subscribe(
          response=>{
            this.loading=false;
            this.toastr.success('Estado pago reseteado correctamente','Success');
          },
          err=>{
            this.loading=false;
            this.toastr.error('No fue posible actualizar','Error');
          }
        );
      };
    })
  }

  public cargarTipoCuota():void{
    this.tipoCuotaService.getAll().subscribe(
      response=>this.tipoCuotas=response
    )
  }

  public mostrarFaltantes(value):void{
    this.cuotaService.findByEstadoCuotaPendiente(value).subscribe(
      response=>{
        this.cuotasPendientes=response;
        this.cuotaService.abrirModal();
      }
    )
  }

  public cerrarModalCuotas():void{
    this.cuotaService.cerrarModal();
  }

  /* *************************************************PDF  */

  public generarPDF(pagoCuota:PagoCuota[]):void{

    pagoCuota.forEach(pc => {
      
    let doc = new jsPDF();
    doc.setFillColor(51, 76, 255);
    doc.rect(0, 0, 220, 6, 'F'); // TOP filled square blue
    doc.setFontSize(12);
    doc.text(`Recibo`, 10, 12);
    doc.setFontSize(10);
    doc.text(`Comité provisional proyecto de drenaje`, 10, 18);
    doc.text('REF', 160, 18);
    doc.line(0,20,200,19);
    doc.text(`NR${pc.id}${pc.cuota.id}${pc.cuota.socio.codigo}${pc.cuota.socio.id}`, 160, 24);
    doc.setFontSize(10);
    doc.line(0,43,200,43)
   
    doc.text(`Nombre: ${pc.cuota.socio.nombre} ${pc.cuota.socio.apellido}`,10,52);
    doc.text(`Descripción: ${pc.cuota.tipoCuota.descripcion}`,10,58);
    const transFecha = this.datePipe.transform(pc.fecha, 'MMMM d, y');  
    doc.text(`Fecha: ${transFecha}, ${pc.hora}`,80,74);
  
    const transGTQ = this.currencyPipe.transform(pc.cantidad,'GTQ')
    doc.setFontSize(12);
    doc.text(`Por: ${transGTQ}`,160,52);
    doc.setFillColor(51, 76, 255);
    doc.rect(0, 100, 220, 6, 'F'); // BOTTOM filled square blue
    doc.save(pc.cuota.socio.nombre+pc.cuota.socio.apellido+'.pdf');
      
    });
    
  }

}
