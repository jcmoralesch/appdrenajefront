import { Component, OnInit, ViewChild } from "@angular/core";
import { GastoService } from "../../../model/service/gasto.service";
import { MatSort, MatPaginator, MatTableDataSource } from "@angular/material";
import Swal from "sweetalert2";
import { Gasto } from "../../../model/class/gasto";
import { ItemGastoService } from "../../../model/service/item-gasto.service";
import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { CurrencyPipe,DatePipe } from "@angular/common";
import { Helper } from "src/app/model/class/helper";
import { InformeSaldoService } from "../../../model/service/informe-saldo.service";
import { ToastrService } from "ngx-toastr";
import { InformeSaldo } from "src/app/model/class/informe-saldo";
import { LoginService } from '../../../model/service/login.service';

@Component({
  selector: "app-list-gasto",
  templateUrl: "./list-gasto.component.html",
  styleUrls: ["./list-gasto.component.css"],
})
export class ListGastoComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource: any;
  public totalGasto: number;
  public gastoEnviar: Gasto;
  private currencyPipe = new CurrencyPipe("es-GT");
  public helper: Helper = new Helper();
  public informeSaldo: InformeSaldo;

  constructor(
    private gastoService: GastoService,
    private itemGastoService: ItemGastoService,
    private informeSaldoService: InformeSaldoService,
    private toastr: ToastrService,
    public loginService:LoginService
  ) {}

  displayedColumns = [
    "Id",
    "NoFactura",
    "Proveedor",
    "Fecha",
    "Hora",
    "TGasto",
    "Acciones",
  ];

  ngOnInit() {
    this.findByCurrentDate();
    this.gastoService.getTotalDisponible().subscribe((response) => {
      this.helper = response;
    });
    this.getInformeSaldo();
    this.notiFicarCambioDesdeInformeSaldo();
  }

  private findByCurrentDate(): void {
    this.gastoService.findByCurrenDate().subscribe((response) => {
      this.totalGasto = response
        .map((t) => t.total)
        .reduce((acc, value) => acc + value, 0);
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = response;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  public consultarGastosRangoFechas(f1, f2): void {
    if (f1 == "" || f2 == "") {
      Swal.fire(
        "No se puede realizar consulta",
        "Ingrese el rango de fechas para realizar la consulta",
        "warning"
      );
    } else {
      this.gastoService.findByFechaBeetween(f1, f2).subscribe((response) => {
        this.totalGasto = response
          .map((t) => t.total)
          .reduce((acc, value) => acc + value, 0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    }
  }

  public abrirModalDetalle(row): void {
    this.gastoEnviar = row;
    this.itemGastoService.abrirModalDetalle();
  }

  public compactarReporte():void{
    Swal.fire({
       title:'Seguro que desea?',
      text: 'Compactar, para que se refleje en el reporte',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI, Compactar !'
    }).then((result) => {

      if (result.value) {
        this.informeSaldoService
      .compactarInformeSaldo()
      .subscribe((response) =>{
        this.informeSaldoService.notificarCambio.emit();
        this.toastr.success("Compactado corretamente", "Success")
      }
      );
      };
    })
  }

  private getInformeSaldo(): void {
    this.informeSaldoService.informeSaldoReporte().subscribe((response) => {
      this.informeSaldo = response;
    });
  }

  private notiFicarCambioDesdeInformeSaldo():void{
    this.informeSaldoService.notificarCambio.subscribe(
      response=>{
        this.getInformeSaldo();
      }
    )
  }

  /**************************************************************************************PDF */
  public generarPDF(): void {
    let doc = new jsPDF();
    doc.setFontSize(10);
    doc.text(`REPORTE GASTOS`, 15, 15);

    for (let j = 0; j < this.dataSource.data.length; j++) {
      let inicio: number;
      let inicio2: number;
      let proveedor: string;

      if (j == 0) {
        inicio2 = 20;
        inicio = 22;
      } else {
        inicio = doc.autoTable.previous.finalY + 10;
        inicio2 = doc.autoTable.previous.finalY + 8;
      }

      if (this.dataSource.data[j].proveedor == null) {
        proveedor = "S/P";
      } else {
        proveedor = this.dataSource.data[j].proveedor.nombreCompleto;
      }

      doc.setFontSize(9);
      var pageSize = doc.internal.pageSize;
      var pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
      var text = doc.splitTextToSize(
        `${this.dataSource.data[j].fecha}. --Proveedor ${proveedor}--`,
        pageWidth - 35,
        {}
      );
      doc.text(text, 15, inicio2);

      doc.autoTable({
        head: this.headRows(),
        body: this.bodyRows(j),
        foot: this.footRows(j),
        startY: inicio,
        pageBreak: "avoid",
        theme: "grid",
        showHead: "firstPage",
        columnStyles: {},
        footStyles: {
          fillColor: [234, 250, 241],
          textColor: [192, 57, 43],
        },
        styles: { cellPadding: 0.5, fontSize: 8 },
      });
    }
    doc.setFontSize(14);
    doc.setTextColor("blue");
    doc.text(
      `Gasto total: ${this.currencyPipe.transform(this.totalGasto, "GTQ")}`,
      120,
      doc.autoTable.previous.finalY + 10
    );

    if (this.informeSaldo != null) {
      doc.autoTable({
        head:this.headRowsInformeSaldo(),
        body: this.bodyRowsInformeSaldo(),
        startY: doc.autoTable.previous.finalY + 30,
        willDrawCell: function (data) {
          if (data.row.section === 'body' && data.column.dataKey === 'saldoEnCaja') {
                doc.setTextColor(21, 67, 96)
                doc.setFontStyle('bold')
          }

          if (data.row.section === 'body' && data.column.dataKey === 'saldoAnterior') {
            doc.setTextColor(21, 67, 96)
            doc.setFontStyle('bold')
      }
        },
        pageBreak: "avoid",
      });
    }

    doc.save(`ReporteGastos`);
  }

  private headRows() {
    return [
      {
        cantidad: "Cant.",
        descripcion: "Descripción",
        precioU: "Precio U.",
        subTotal: "Sub-Total",
      },
    ];
  }

  private bodyRows(indice: number) {
    let body = [];
    let contador: number = 0;
    let t = this.dataSource.data[indice].items.length;
    for (var j = 1; j <= t; j++) {
      body.push({
        cantidad: this.dataSource.data[indice].items[contador].cantidad,
        descripcion: this.dataSource.data[indice].items[contador].descripcion,
        precioU: this.currencyPipe.transform(
          this.dataSource.data[indice].items[contador].precioUnitario,
          "GTQ"
        ),
        subTotal: this.currencyPipe.transform(
          this.dataSource.data[indice].items[contador].subTotalCompra,
          "GTQ"
        ),
      });
      contador++;
    }
    return body;
  }

  private headRowsInformeSaldo() {
    return [
      {
        fecha: "Fecha última lectura",
        saldoAnterior: "Saldo Anterior",
        totalRecaudo: "Total recaudo",
        totalGasto: "Total gastos",
        saldoEnCaja:'Saldo en caja'
      },
    ];
  }

  private bodyRowsInformeSaldo() {
    let body = [];
    let datePipe = new DatePipe('es-GT');

    body.push({
      fecha: datePipe.transform(this.informeSaldo.fechaLectura, 'EEEE dd, MMMM yyyy').toUpperCase(),
      saldoAnterior: this.currencyPipe.transform(this.informeSaldo.saldoAnterior,'GTQ'),
      totalRecaudo: this.currencyPipe.transform(this.informeSaldo.totalRecaudo,'GTQ'),
      totalGasto: this.currencyPipe.transform(this.informeSaldo.totalGasto,'GTQ'),
      saldoEnCaja: this.currencyPipe.transform(this.informeSaldo.saldoEnCaja,'GTQ'),
    });

    return body;
  }

  private footRows(indice: number) {
    return [
      {
        cantidad: "",
        descripcion: "",
        precioU: "",
        subTotal: this.currencyPipe.transform(
          this.dataSource.data[indice].total,
          "GTQ"
        ),
      },
    ];
  }
}