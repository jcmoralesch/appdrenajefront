import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FechaPagosService {

  public modal:boolean=false;

  constructor() { }

  public abrirModalFechas():void{
    this.modal=true;
  }

  public cerrarModalFechas():void{
    this.modal=false;
  }
}
