import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/usuario/login/login.component';
import { UsuarioComponent } from './components/usuario/usuario/usuario.component';
import { AuthGuard } from './components/usuario/guards/auth.guard';
import { FormUsuarioComponent } from './components/usuario/form-usuario/form-usuario.component';
import { RoleGuard } from './components/usuario/guards/role.guard';
import { FormSocioComponent } from './components/socios/form-socio/form-socio.component';
import { ListSociosComponent } from './components/socios/list-socios/list-socios.component';
import { ListCuotaComponent } from './components/cuotas/list-cuota/list-cuota.component';
import { FormCuotaComponent } from './components/cuotas/form-cuota/form-cuota.component';
import { ListTipoCuotaComponent } from './components/tipoCuota/list-tipo-cuota/list-tipo-cuota.component';
import { TipoCuotaFormComponent } from './components/tipoCuota/tipo-cuota-form/tipo-cuota-form.component';
import { FormPagoComponent } from './components/pagos/form-pago/form-pago.component';
import { ListGastoComponent } from './components/gastos/list-gasto/list-gasto.component';
import { FormGastoComponent } from './components/gastos/form-gasto/form-gasto.component';
import { HistorialPagoComponent } from './components/pagos/historial-pago/historial-pago.component';
import { InformeSaldoComponent } from './components/informe-saldo/informe-saldo.component';

const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'usuario',component:UsuarioComponent,canActivate:[AuthGuard,RoleGuard],data:{role:'ROLE_ADMIN'}},
  {path:'form-usuario',component:FormUsuarioComponent, canActivate:[AuthGuard],data:{role:'ROLE_ADMIN'}},
  {path:'form-socio',component:FormSocioComponent,canActivate:[AuthGuard,RoleGuard],data:{role:'ROLE_ADMIN'}},
  {path:'socio/form/:id',component:FormSocioComponent,canActivate:[AuthGuard,RoleGuard],data:{role:'ROLE_ADMIN'}},
  {path:'socio',component:ListSociosComponent,canActivate:[AuthGuard]},
  {path:'cuota',component:ListCuotaComponent,canActivate:[AuthGuard]},
  {path:'cuota-form',component:FormCuotaComponent,canActivate:[AuthGuard,RoleGuard],data:{role:'ROLE_ADMIN'}},
  {path:'tipo-cuota',component:ListTipoCuotaComponent,canActivate:[AuthGuard]},
  {path:'form-tipo-cuota',component:TipoCuotaFormComponent,canActivate:[AuthGuard,RoleGuard],data:{role:'ROLE_ADMIN' }},
  {path:'pago',component:FormPagoComponent,canActivate:[AuthGuard]},
  {path:'pagos/historial-pago',component:HistorialPagoComponent,canActivate:[AuthGuard]},
  {path:'gasto',component:ListGastoComponent,canActivate:[AuthGuard]},
  {path:'gasto-form',component:FormGastoComponent,canActivate:[AuthGuard,RoleGuard],data:{role:'ROLE_ADMIN' }},
  {path:'informe-saldo',component:InformeSaldoComponent},
  {path:'**',pathMatch:'full', redirectTo:'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
