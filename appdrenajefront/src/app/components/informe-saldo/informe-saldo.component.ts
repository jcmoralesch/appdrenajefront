import { Component, OnInit } from '@angular/core';
import { InformeSaldoService } from '../../model/service/informe-saldo.service';
import { InformeSaldo } from 'src/app/model/class/informe-saldo';

@Component({
  selector: 'app-informe-saldo',
  templateUrl: './informe-saldo.component.html',
  styleUrls: ['./informe-saldo.component.css']
})
export class InformeSaldoComponent implements OnInit {

  public informeSaldos:InformeSaldo[];

  constructor(private informeSaldoService:InformeSaldoService) { }

  ngOnInit(): void {
    this.getAllOrderByDesc();
  }

  private getAllOrderByDesc():void{
    this.informeSaldoService.getAllOrderByDesc().subscribe(
      response=>this.informeSaldos=response
    )
  }

}
