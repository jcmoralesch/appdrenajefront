import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Socio } from '../../../model/class/socio';
import { SocioService } from '../../../model/service/socio.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { SectorService } from '../../../model/service/sector.service';
import { Sector } from '../../../model/class/sector';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-form-socio',
  templateUrl: './form-socio.component.html',
  styleUrls: ['./form-socio.component.css']
})
export class FormSocioComponent implements OnInit {

  public loading:boolean=false;
  public socio:Socio = new Socio();
  public sexos:string[] = ['Hombre','Mujer'];
  public sectors:Sector[];
  private idActual:number;
   
  constructor(private socioService:SocioService,
              private router:Router,
              private toastr:ToastrService,
              private sectorService:SectorService,
              private  activatedRoute:ActivatedRoute){}

  ngOnInit() {
    this.getAllSector();
    this.cargarSocioDesdeFormulario();
  }


  public store(forma:NgForm):void{

    this.loading=true;
    this.socioService.store(this.socio).subscribe(
      socio=>{
        this.socio=socio;
        this.router.navigate(['/socio']);
        this.toastr.success('Socio registrado con exito','Exito');
        this.loading=false;
      },
      err=>{
        this.loading=false;
      }
    )    

  }

  private getAllSector():void{
    this.sectorService.getAll().subscribe(
      sector=>this.sectors=sector
    )
  }

  private cargarSocioDesdeFormulario():void{
    this.activatedRoute.params.subscribe(
      params=>{
        let id=params['id'];
         
        if(id){
          this.socioService.getById(id).subscribe(
            response=>{
              this.socio=response;
              this.idActual=response.id;
            }
          )
        }
      }
    )

  }

  compararSector(o1:Sector,o2:Sector):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

  public update():void{
    this.loading=true;
    this.socioService.update(this.socio).subscribe(
      response=>{
        this.loading=false;
        this.router.navigate(['/socio']);
        Swal.fire('Exito','Datos del socio actualizado correctamente','success');
      },
      err=>{
          this.loading=false;
          Swal.fire('Error','No fue posible actualizar los datos','error');
      }
    )
  }
}


