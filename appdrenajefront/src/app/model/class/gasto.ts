import { ItemGasto } from './item-gasto';
import { Proveedor } from './proveedor';

export class Gasto {

    public noFactura:string;
    public items:Array<ItemGasto>=[];
    public proveedor:Proveedor;
    public total:number;
    public fecha;

}
