import { Component, OnInit, ViewChild } from '@angular/core';
import { CuotaService } from '../../../model/service/cuota.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Cuota } from 'src/app/model/class/cuota';
import { TipoCuota } from '../../../model/class/tipo-cuota';
import { TipoCuotaService } from '../../../model/service/tipo-cuota.service';
import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { CurrencyPipe } from '@angular/common';
import { LoginService } from '../../../model/service/login.service';

@Component({
  selector: 'app-list-cuota',
  templateUrl: './list-cuota.component.html',
  styleUrls: ['./list-cuota.component.css']
})
export class ListCuotaComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource:any;
  public tipoCuotas:TipoCuota[];
  private cuotasPendiente:Cuota[];
  private tipoCuotaPdf:string='';
 
  constructor(private cuotaService:CuotaService,
              private tipoCuotaService:TipoCuotaService,
              public loginService:LoginService) { }

  ngOnInit() {
    this.findAllByEstadoCuenta();
    this.getAllTipoCuota();
  }

  displayedColumns = ['Codigo','Nombre','Apellido','Cantidad','NoPagos','TotalPagado','Restante','TipoCuota'];
 
  public findAllByEstadoCuenta():void{
    this.cuotaService.findByEstadoCuota().subscribe(
      response=>{
        this.dataSource= new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();
      }
    )
  }

  applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter = filterValue;
  }

  public tableFilter(): (data: Cuota, filter: string) => boolean {
    let filterFunction = function (data, filter): boolean {
      return (
        data.socio.nombre.indexOf(filter) != -1 
        || data.socio.apellido.indexOf(filter) != -1
      );
    };
    return filterFunction;
  }

  private getAllTipoCuota():void{
    this.tipoCuotaService.getAll().subscribe(
      response=>this.tipoCuotas=response
    )
 
  }

  public findAllByTipoCuota(value:string):void{
    this.tipoCuotaPdf=value;
    this.cuotasPendiente=[];
    this.cuotaService.findByTipoCuota(value).subscribe(
      response=>{
        this.dataSource= new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();
        this.cuotasPendiente=response;
      }
    )
  }

  public abrirModal():void{

  }

  /*****************************PDF Restante***************************** */


  generarPDFRestante() {
    let doc = new jsPDF();
    doc.setFontSize(10);
    doc.text(`Listado de usuarios pendientes de pago de la cuota: ${this.tipoCuotaPdf}`,15, 15);
    
    doc.autoTable({
      head: this.headRows(),
      body: this.bodyRows(),
      startY: 17,
      theme:'grid',
      showHead: "firstPage",
      styles: { cellPadding: 0.5, fontSize: 8 }
    });
    doc.save(`UsuariosPendienteCuota`);
  }

  private   headRows() {
    return [
      {
        numero:'#',
        nombre: "Nombre",
        apellido: "Apellido",
        cuota:"Cuota",
        restante:"Restante",
        tPagado:"T. pagado",
        opcion4:"----"
      }
    ];
  }

   bodyRows() {
    let body = [];
    let contador: number = 0;
    let t = this.cuotasPendiente.length;
    let currencyPipe= new CurrencyPipe('es-GT')
    for (var j = 1; j <= t; j++) {
      body.push({
        numero:j,
        nombre: this.cuotasPendiente[contador].socio.nombre,
        apellido:this.cuotasPendiente[contador].socio.apellido,
        cuota: currencyPipe.transform(this.cuotasPendiente[contador].cantidad,'GTQ'),
        restante: currencyPipe.transform(this.cuotasPendiente[contador].restante,'GTQ'),
        tPagado: this.cuotasPendiente[contador].totalPagado,
        opcion4:' '

      });
      contador++;
    }
    return body;
  }


  generarPDFCompleto(tipoCuota:string){
    let totalC:number=0;
    this.cuotaService.findAllByTipoCuota(tipoCuota).subscribe(
      response=>{
        response.forEach((c:Cuota)=>{
          totalC+=c.totalPagado;
        });
        this.generarReporteCompleto(response,totalC);
        
      }
    );
  }

  private generarReporteCompleto(cuota:Cuota[], total:number){
    let doc = new jsPDF();
    doc.setFontSize(10);
    doc.text(`Listado completo de usuarios de la cuota: ${this.tipoCuotaPdf}`,15, 15);
    
    doc.autoTable({
      head: this.headRows(),
      body: this.bodyRowsCompleto(cuota),
      foot: this.footRows(total),
      startY: 17,
      theme:'grid',
      showHead: "firstPage",
      willDrawCell: function(data) {
        if (data.row.section === "body"  && data.column.dataKey === "tPagado") {
          if(data.cell.raw==0)
          {
             doc.setFillColor(245, 183, 177)
             doc.setFontStyle('bold');
          }
          if(data.cell.raw==cuota[0].cantidad)
          {
             doc.setFillColor(171, 235, 198)
             doc.setFontStyle('bold');
          }
        }
      },
      columnStyles: {
        restante:{
          fontStyle: 'bold'
        },
        tPagado:{
          fontStyle: 'bold'
        }
    },
      styles: { cellPadding: 0.5, fontSize: 8 }
    });
    doc.save(`ListadoCompletoUsuarios`);
  }

  private bodyRowsCompleto(ct:Cuota[]) {
    let body = [];
    let contador: number = 0;
    let t = ct.length;
    let currencyPipe= new CurrencyPipe('es-GT')
    for (var j = 1; j <= t; j++) {
      body.push({
        numero:j,
        nombre: ct[contador].socio.nombre,
        apellido:ct[contador].socio.apellido,
        cuota: currencyPipe.transform(ct[contador].cantidad,'GTQ'),
        restante:currencyPipe.transform( ct[contador].restante,'GTQ'),
        tPagado: ct[contador].totalPagado,
        opcion4:' '

      });
      contador++;
    }
    return body;
  }

  private   footRows(total:number) {
    let currencyPipe= new CurrencyPipe('es-GT')
    return [
      {
        numero:'',
        nombre: "",
        apellido: "",
        cuota:"Total",
        restante:"Recaudo:",
        tPagado:currencyPipe.transform(total,'GTQ'),
        opcion4:""
      }
    ];
  }

}
