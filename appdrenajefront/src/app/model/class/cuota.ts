import { TipoCuota } from './tipo-cuota';
import { Socio } from './socio';


export class Cuota {

    public cantidad:number;
    public noPagos:number;
    public tipoCuota:TipoCuota;
    public socio:Socio;
    public id:number;
    public restante:number;
    public totalPagado:number=0;
       
}
