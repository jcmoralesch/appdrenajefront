import { Sector } from './sector';
export class Socio {
  
    public codigo:number;
    public nombre:string='';
    public apellido:string='';
    public sexo:string='';
    public telefono:string='';
    public sector:Sector;
    public id:number;
}
