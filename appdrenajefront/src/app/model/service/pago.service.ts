import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../../config/config';
import { PagoCuota } from '../class/pago-cuota';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Socio } from '../class/socio';

@Injectable({
  providedIn: 'root'
})
export class PagoService {

  private urlEndPoint:string=URL_BACKEND+'/api-drenaje/pago-cuota';
  private _notificarCambio= new EventEmitter<any>();

  constructor(private http:HttpClient) { }

  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  public store(pagoCuota:Array<PagoCuota>):Observable<PagoCuota[]>{
    return this.http.post<PagoCuota[]>(`${this.urlEndPoint}`,pagoCuota).pipe(
      map((response:any)=>response.pagoCuota as PagoCuota[]),
      catchError( 
        err=>{
          if(err.status==400){
            return throwError(err)
          }
          Swal.fire(err.error.mensaje,err.error.err,'error');
          return throwError(err);
        }
      )
    )
  }

  public findByCurrentDate():Observable<PagoCuota[]>{
    return this.http.get<PagoCuota[]>(`${this.urlEndPoint}`);
  }

  public findByfechaBeetwenfecha(fecha1:string,fecha2:string,tipoCuota:string):Observable<PagoCuota[]>{
    return this.http.get<PagoCuota[]>(`${this.urlEndPoint}/find/${fecha1}/${fecha2}/${tipoCuota}`);
  }

  public findAllBySocio(socio:Socio):Observable<PagoCuota[]>{
    return this.http.post<PagoCuota[]>(`${this.urlEndPoint}/find/by/socio`, socio);
  }

}
