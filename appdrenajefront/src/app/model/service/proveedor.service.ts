import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../../config/config';
import { Proveedor } from '../class/proveedor';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  public modal:boolean=false;
  private urlEndPoint:string =URL_BACKEND+'/api-drenaje/proveedor';
  private _notificarCambio= new EventEmitter<any>();

  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  constructor(private http:HttpClient) { }

  public store(proveedor:Proveedor):Observable<Proveedor>{
    return this.http.post<Proveedor>(`${this.urlEndPoint}`,proveedor).pipe(
      map((response:any)=>response.proveedor as Proveedor),
      catchError(
        err=>{
          if(err.status==400){
            return throwError(err);
          }
          Swal.fire(err.error.mensaje,err.error.err,'error');
          return throwError(err);
        })
    )
  }

  public filtrarProveedores(term:string):Observable<Proveedor[]>{
    return this.http.get<Proveedor[]>(`${this.urlEndPoint}/filtrar-proveedor/${term}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
