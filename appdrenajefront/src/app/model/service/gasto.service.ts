import { Injectable, EventEmitter } from '@angular/core';
import { URL_BACKEND } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { Gasto } from '../class/gasto';
import { Observable,throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { DatePipe} from '@angular/common';
import { Helper } from '../class/helper';

@Injectable({
  providedIn: 'root'
})
export class GastoService {

  private urlEndPoint:string=URL_BACKEND+'/api-drenaje/gasto';
  private _notificarCambio= new EventEmitter<any>();

  constructor(private http:HttpClient) { }

  public get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  public store(gasto:Gasto):Observable<Gasto>{
    return this.http.post<Gasto>(`${this.urlEndPoint}`,gasto).pipe(
        map((response:any)=>response.gasto as Gasto),
        catchError(err=>{
          if(err.status==400){
            return throwError(err);
          }
          Swal.fire(err.error.mensaje,err.error.err,'error');
          return throwError(err);
        })
    )
  }

  public findByCurrenDate():Observable<Gasto[]>{
    return this.http.get<Gasto[]>(`${this.urlEndPoint}/curren-date`);
  }

  public findByFechaBeetween(fecha1:string,fecha2:string):Observable<Gasto[]>{
    return this.http.get<Gasto[]>(`${this.urlEndPoint}/find/${fecha1}/${fecha2}`).pipe(
      map((response)=>{
        (response).map(gasto=>{
          let datePipe = new DatePipe('es-GT');
          gasto.fecha = datePipe.transform(gasto.fecha, 'EEEE dd, MMMM yyyy').toUpperCase();
          gasto.items.map(itemGasto=>{
            itemGasto.descripcion=itemGasto.descripcion.toUpperCase();
            return itemGasto;
          })
          return gasto;
        })
        return response;
      })
    );
  }

  public getTotalDisponible():Observable<Helper>{
    return this.http.get<Helper>(`${this.urlEndPoint}/consultar/disponible`);
  }
}
