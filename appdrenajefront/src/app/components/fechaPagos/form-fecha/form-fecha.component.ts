import { Component, OnInit } from '@angular/core';
import { FechaPagosService } from '../../../model/service/fecha-pagos.service';

@Component({
  selector: 'app-form-fecha',
  templateUrl: './form-fecha.component.html',
  styleUrls: ['./form-fecha.component.css']
})
export class FormFechaComponent implements OnInit {

  constructor(public fechaPagosService:FechaPagosService) { }

  ngOnInit() {
  }

  public cerrarModal():void{
    this.fechaPagosService.cerrarModalFechas();
  }

}
