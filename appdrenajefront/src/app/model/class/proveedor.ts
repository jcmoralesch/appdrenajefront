export class Proveedor {
    public nombreCompleto:string='';
    public id:number;
    public descripcion:string;
    public direccion:string;
    public telefono:string;
}
