import { Cuota } from './cuota';
export class PagoCuota {

      public cantidad:number=0;
      public cuota:Cuota;
      public fecha;
      public hora:String;
      public id:number;
}
