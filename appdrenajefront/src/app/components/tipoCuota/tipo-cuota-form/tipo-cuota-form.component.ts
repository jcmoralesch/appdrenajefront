import { Component, OnInit } from '@angular/core';
import { TipoCuotaService } from '../../../model/service/tipo-cuota.service';
import { TipoCuota } from '../../../model/class/tipo-cuota';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { FechaPagosService } from '../../../model/service/fecha-pagos.service';
import { FechasPago } from '../../../model/class/fechas-pago';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tipo-cuota-form',
  templateUrl: './tipo-cuota-form.component.html',
  styleUrls: ['./tipo-cuota-form.component.css']
})
export class TipoCuotaFormComponent implements OnInit {

  public tipoCuota:TipoCuota = new TipoCuota();
  public loading:false;
  public fechaPagos:FechasPago = new FechasPago();

  constructor(public tipoCuotaService:TipoCuotaService,
              private toastrService:ToastrService,
              public fechaPagosService:FechaPagosService,
              private router:Router) { }

  ngOnInit() {
  }

  public store():void{
    this.tipoCuotaService.store(this.tipoCuota).subscribe(
      tipoCuota=>{
        this.router.navigate(['/cuota-form'])
        this.toastrService.success('Tipo Cuota registrado con exito','Success');
      },
      err=>{
        Swal.fire('Error','No fue posible registrar el tipo de pago','error');
      }
    )
  }

  public agregarFechas(): void {
      this.tipoCuota.items.push(this.fechaPagos);
      console.log(this.fechaPagos);
      this.fechaPagosService.cerrarModalFechas();
      this.fechaPagos = new FechasPago();
  }


  eliminarItemFecha(id:string): void {
    this.tipoCuota.items = this.tipoCuota.items.filter((item: FechasPago) => id !== item.fechaPago);
  }

  public cerrarModalFechas():void{
    this.fechaPagosService.cerrarModalFechas();
  }


  public abrirModalFechas():void{
    this.fechaPagosService.abrirModalFechas();
  }
}
