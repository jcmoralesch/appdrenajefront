import { Component, OnInit, Input } from '@angular/core';
import { ItemGastoService } from '../../model/service/item-gasto.service';
import { Gasto } from '../../model/class/gasto';

@Component({
  selector: 'app-item-gasto',
  templateUrl: './item-gasto.component.html',
  styleUrls: ['./item-gasto.component.css']
})
export class ItemGastoComponent implements OnInit {

  @Input() gastoRecibir:Gasto;

  constructor(public itemGastoService:ItemGastoService) { }

  ngOnInit() {
  }

  public cerrarModal():void{
      this.itemGastoService.cerrarModalDetalle();
  }

}
